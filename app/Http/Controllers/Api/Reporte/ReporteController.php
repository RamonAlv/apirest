<?php

namespace App\Http\Controllers\Api\Reporte;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Reporte;

class ReporteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reporte = Reporte::all();

        return response()->json(['data'=>$reporte], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reporte = new Reporte();

        $reporte->title = $request->title;
        $reporte->description = $request->description;
        // $reporte->user_id = $request->input('user_id');
        
        $reporte->saveOrFail();

        if($request->has('image')){
            $file = $request->file('image');
            $path = public_path() . '/images/reportes/' . $reporte->id . '/'; // el ' . ' es para concatenar en php
            $fileName = uniqid() . $file->getClientOriginalName();
            $moved = $file->move($path, $fileName);

            if($moved){
                $reporte->image = '/images/reportes/' . $reporte->id . '/' . $fileName;
                $reporte->saveOrFail();
            }
        }
        return response()->json(['data' => $reporte], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = null;
        try{

            $reporte = Reporte::findOrFail($id);
            $msj = "Respuesta Exitosa, code 200";
            $response = array(
                'data'=>$reporte
                // 'response'=>$msj,
                // 'status'=>200
            );
        }catch(Exception $e){
            $response = array(
                'data'=>'',
                'response'=>$e->getMessage(),
                'status'=>500
            );
        }
        return response()->json($response, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $reporte->title = $request->title;
        // $reporte->description = $request->description;
        // $reporte->saveOrFail();

        // return response()->json(['data' => $reporte], 201);
        $response = null;
        try{

            $reporte = Reporte::findOrFail($id);
            $reporte->title = $request->title;
            $reporte->description = $request->description;
            $reporte->save;
            $msj = "Reporte Editado exitosamente";
            $response = array(
                'data'=>$reporte,
                'response'=>$msj,
                // 'status'=>200
            );
        }catch(Exception $e){
            $response = array(
                'data'=>'',
                'response'=>$e->getMessage(),
                'status'=>500
            );
        }
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $reporte -> delete();
        // $value = "El reporte fue eliminado exitosamente";
        // return response()->json(['data' => $value], 200);
        $response = null;
        try{

            $reporte = Reporte::findOrFail($id);
            $reporte -> delete();
            $msj = "Reporte eliminado exitosamente";
            $response = array(
                'data'=>$msj
                // 'response'=>$msj,
                // 'status'=>200
            );
        }catch(Exception $e){
            $response = array(
                'data'=>'',
                'response'=>$e->getMessage(),
                'status'=>500
            );
        }
        return response()->json($response, 200);
    }
}
