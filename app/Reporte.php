<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reporte extends Model
{
    protected $table = 'reportes';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'title','description','image'
    ];

    public function scopeReporte($query){

        return $query->orderBy('id','desc');

    }


    protected $hidden = [
        'updated_at', 'created_at'
    ];

    protected $dates = ['deleted_at'];
}
